<?php

/**
 * @file
 * Mocean configuration page.
 */

/**
 * Page callback for the Mocean configuration form.
 */
function mocean_settings_form($form, $form_state) 
{
	$form['mocean_api_key'] = array(
		'#type' => 'textfield',
		'#title' => t('API key'),
		'#required' => TRUE,
		'#default_value' => variable_get('mocean_api_key', ''),
		'#maxlength' => 15,
		'#description' => t('Maximun of 15 characters in length.'),
	);

	$form['mocean_api_secret'] = array(
		'#type' => 'textfield',
		'#title' => t('API Secret'),
		'#required' => TRUE,
		'#default_value' => variable_get('mocean_api_secret', ''),
	);
	
	$form['mocean_credit_balance_warning'] = array(
		'#type' => 'textfield',
		'#title' => t('Credit balance warning'),
		'#description' => t('Display a warning on the status report if the credit balance is below this amount. Leave empty to disable the warning.'),
		'#element_validate' => array('element_validate_integer_positive'),
		'#default_value' => variable_get('mocean_credit_balance_warning', ''),
	);

	$form['mocean_debug_mode'] = array(
		'#type' => 'checkbox',
		'#title' => t('Debug mode'),
		'#description' => t('Debug mode displays the message data every time a message is sent.'),
		'#default_value' => variable_get('mocean_debug_mode', 0),
	);

	return system_settings_form($form);
}

/**
 * Page callback for the Mocean API testing form.
 */
function mocean_send_form($form, $form_state) 
{
	$form['sender'] = array(
		'#type' => 'textfield',
		'#title' => t('Sender'),
		'#required' => TRUE,
		'#description' => t('<li>Phone number must include country code, for example, a Malaysian phone number will be like 60123456789.</li>'),
	);

	$form['to'] = array(
		'#type' => 'textfield',
		'#title' => t('To'),
		'#required' => TRUE,
		'#description' => t('<li>To send to multiple receivers, separate each entry with white space (‘ ’) or comma (,).</li><li>Phone number must include country code, for example, a Malaysian phone number will be like 60123456789.</li>'),
	);

	$form['message_body'] = array(
		'#type' => 'textarea',
		'#title' => t('Message Body'),
		'#required' => TRUE,
	);

	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Send message'),
	);

	return $form;
}

/**
 * Submit handler for the Mocean API testing form.
 */
function mocean_send_form_submit($form, &$form_state) 
{
	$message_sent = mocean_send($form_state['values']['message_body'], $form_state['values']['sender'], $form_state['values']['to']);

	if ($message_sent) 
	{
		drupal_set_message(t('The message was successfully sent.'));
	}
	else 
	{
		drupal_set_message(t('An error occured while sending the message. Check the logs for details.'), 'error');
	}
}
