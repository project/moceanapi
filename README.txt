--------------------------------------------------------------------------------
                                 Mocean
--------------------------------------------------------------------------------

The Mocean module provides integration with the Mocean cloud communication
platform allowing for your Drupal site to integration SMS functionality. 

The module currently provides the following functionality:

- Rules event to act on incoming SMS message
- Rules action to send an SMS message
- Hook for incoming SMS messages
- Uses libraries api to manage the Mocean php library

Project homepage: https://www.drupal.org/project/moceanapi

Dependencies
------------

* Libraries API - http://www.drupal.org/project/libraries

The Rules module (http://www.drupal.org/project/rules) is required to use the
rules integration.


Installation
------------
1. Install the Mocean module as per (http://drupal.org/node/895232/)
2. Refer to Mocean documentation to download the Mocean php library from (http://moceanapi.com/docs/?php#installation).
3. Extract the library in your sites/all/libraries folder and rename the directory to 'mocean'.
4. Visit 'admin/config/system/moceanapi' and enter your Mocean API information found on the Mocean dashboard 


Documentation
-------------
* Documentation: http://moceanapi.com/docs/?php#installation
