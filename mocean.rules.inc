<?php

/**
 * @file
 * Provides Rules integration for Mocean.
 */

/**
 * Implements hook_rules_action_info().
 */
function mocean_rules_action_info() 
{
	return array(
		'mocean_send' => array(
		  'label' => t('Send a SMS message'),
		  'parameter' => array(
			'message_body' => array(
			  'type' => 'text',
			  'label' => t('Message Body'),
			),
			'sender' => array(
			  'type' => 'text',
			  'label' => t('Sender'),
			),
			'to' => array(
			  'type' => 'text',
			  'label' => t('To'),
			),
		  ),
		  'group' => t('Mocean'),
		),
	);
}
